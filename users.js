const mongoose = require('mongoose');

//Create a user model
//answer
const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is required']
	},
	lastName: {
		type: String,
		required: [true, 'Last name is required']
	},
	email: {
		type: String,
		required: [true, 'Email is required']
	},
 	password: {
 		type: String
 		required: [true, 'Password is required']
 	},
	isAdmin: {
		type: String, //but supposedly Boolean
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile Number is required']
	},
	enrollments: [
		{
		courseId: {
			type: String
		},
		enrolledOn: {
			type: Date, 
			Default: new Date()
		},
		status: {
			type: String,
			Default: Enrolled	
		}
	}
	]
})


module.exports = mongoose.model('User', userSchema);