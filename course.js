const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema({
	//Data for our fields/proeperties to be included when creating a record
	//The 'true' value defines if the
	name: {
		type: String,
		required: [true, 'Course is required']
	},
	description: {
		type: String,
		required: [true, 'Description is required']
	},
	price: {
		type: Number,
		required: [true, 'Price is required']
	}
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		//the new Date() expression instantiates a new date that stores the current date and time whenever a coruse is created in our database.
		default: new Date()
	},
	//we will be applying the concept of referencing data to establish a realationship between our courses and user.
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, 'UserId is required']
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}

	]
})

module.exports = mongoose.model('Course', courseSchema);